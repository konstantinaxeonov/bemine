import "@styles/vuetify/main.scss";
import { createVuetify } from "vuetify";
import { VBtn } from "vuetify/components/VBtn";
import bemineTheme from "@/vuetify/themes/bemine";

export enum ThemeNames {
  bemineTheme = "bemineTheme",
}

const vuetify = createVuetify({
  aliases: {
    PrimaryButton: VBtn,
  },
  defaults: {
    PrimaryButton: {
      class: ["bg-yellow-300"],
      variant: "flat",
      minWidth: "200px",
      ripple: false,
    },
  },
  theme: {
    defaultTheme: "bemineTheme",
    themes: {
      bemineTheme: bemineTheme,
    },
  },
  display: {
    mobileBreakpoint: "lg",
    thresholds: {
      xs: 0,
      sm: 375,
      md: 768,
      lg: 1024,
      xl: 1440,
      xxl: 1920,
    },
  },
});

export default vuetify;
