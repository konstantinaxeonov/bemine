import { add } from "@/helpers/test-helper/testHelper.ts";

it("adds 1 + 2 to equal 3", () => {
  expect(add(1, 2)).toBe(3);
});
