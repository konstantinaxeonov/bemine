import { mount } from "@vue/test-utils";
import HelloWorld from "./HelloWorld.vue";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

describe("HelloWorld", () => {
  const vuetify = createVuetify({ components, directives });

  test("should display header text", async () => {
    const msg = "Hello Vue 3";

    const wrapper = mount(HelloWorld, {
      // shallow: true,
      props: { msg },
      global: {
        plugins: [
          vuetify,
          // pinia
        ],
      },
    });

    expect(wrapper.find("h1").text()).toEqual(msg);
  });
});
