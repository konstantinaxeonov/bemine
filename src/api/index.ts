import memoizeOne from "memoize-one";
import { stringify } from "qs";
import axios, * as Axios from "axios";
import { requestLogger, responseLogger, errorLogger } from "axios-logger";
import { API_BASE_URL } from "@/config";

const DEFAULT_REQUEST_TIMEOUT = 10000;

export const URLS = {
  TEST: "posts",
};

function getApi(config: Axios.AxiosRequestConfig = {}) {
  const instance = axios.create({
    baseURL: `${API_BASE_URL}api/`,
    timeout: DEFAULT_REQUEST_TIMEOUT,
    paramsSerializer: {
      encode: (params) => {
        return stringify(params, { arrayFormat: "repeat" });
      },
    },
    ...config,
  });

  //@ts-ignore
  if (import.meta.env.MODE !== "production") {
    // логи только для static & test
    instance.interceptors.request.use(
      (internalRequest: Axios.InternalAxiosRequestConfig) => {
        const headers = internalRequest.headers;
        const request: Axios.AxiosRequestConfig = {
          ...internalRequest,
        };
        const logRequest = requestLogger(request);
        internalRequest = {
          ...logRequest,
          ...{ headers },
        };
        return internalRequest;
      },
      (error) => {
        // write down your error intercept.
        return errorLogger(error);
      }
    );
    instance.interceptors.response.use(responseLogger, (error) => {
      // write down your error intercept.
      return errorLogger(error);
    });
  }

  return new Proxy(
    {
      get get() {
        return instance.get;
      },
      get post() {
        return instance.post;
      },
      get put() {
        return instance.put;
      },
      get patch() {
        return instance.patch;
      },
      get delete() {
        return instance.delete;
      },
      async all(urls: string[]) {
        return await axios.all(urls.map((url) => instance.get(url)));
      },
    },
    {
      get(target, p) {
        return target[p as keyof typeof target].bind(instance);
      },
    }
  );
}

export const useApi = memoizeOne(getApi) as typeof getApi;
