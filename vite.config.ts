import { defineConfig } from "vitest/config";
import vue from "@vitejs/plugin-vue";
import svgLoader from "vite-svg-loader";
import { viteStaticCopy } from "vite-plugin-static-copy";
import path from "path";
import Components from "unplugin-vue-components/vite";
import { Vuetify3Resolver } from "unplugin-vue-components/resolvers";
import vuetify from "vite-plugin-vuetify";

const paths = {
  src: path.resolve(__dirname, "src"),
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vuetify({
      styles: { configFile: "src/assets/styles/vuetify/variables.scss" },
    }),
    svgLoader(),
    Components({
      resolvers: [Vuetify3Resolver()],
    }),
    viteStaticCopy({
      targets: [
        {
          src: "src/static/",
          dest: "",
        },
      ],
    }),
  ],
  resolve: {
    alias: {
      "@": paths.src,
      "@styles": paths.src + "/assets/styles",
    },
  },
  test: {
    globals: true,
    environment: "jsdom",
    deps: {
      inline: [/vuetify/],
    },
  },
  server: {
    host: "0.0.0.0",
    port: 3000,
    proxy: {
      "^/api": {
        target: "https://jsonplaceholder.typicode.com/",
        changeOrigin: true,
        // changeOrigin заменяет только host, поэтому у конченого запроса остается /api/
        // rewrite помогает убрать /api/ из конечного запроса
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
});
