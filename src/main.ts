import { createApp } from "vue";
import App from "./App.vue";
import vuetify from "@/vuetify";
import "@styles/core/index.scss";

createApp(App).use(vuetify).mount("#app");
